import Layout from '../components/Layout'

const contactTitle = "Contact | IANEWBIE.COM"
export default () => (
	<div>
		<Layout title={contactTitle}>
			<p>It's Contact Page</p>
		</Layout>
	</div>
)