import Layout from '../components/Layout'

const aboutTitle = "About US"
export default () => (
	<div>
		<Layout title={aboutTitle}>
			<p>It's About page</p>
		</Layout>
	</div>
)