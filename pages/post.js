import Layout from '../components/Layout'
import ReactMarkdown from 'react-markdown'
import posts from '../services/contentconf'

const Post = (props) => (
		<Layout title={props.single_post.fields.productName}>
		<div className="content">
		<div className="post_header">
			<h1>{props.single_post.fields.productName}</h1>
		</div>
			<ReactMarkdown source={props.single_post.fields.productDescription} />
			<div className="tab_list">
				<ul>
					{props.single_post.fields.tags.map((tag, index) =>(
							<li key={index}>{tag}</li>
					))}
				</ul>
			</div>
			<style jsx global>{`
				.post_header {
					margin-top:40px;
				}
				.post_header h1 {
					font-size: 40px;	
					font-weight: 700;	
					margin:0;	
					font-family: 'Vollkorn', serif;		
				}
				.content {
					width: 60%;
					margin:0px auto;
				}
				.content img {
					max-width: 100%;
					-webkit-box-shadow:5px 5px 5px -1px rgba(0, 0, 0, 0.2);
					box-shadow:5px 5px 5px -1px rgba(0, 0, 0, 0.2);
					margin:40px 0px;
				}
				.content {
					font-size:14px;
				}
				.content pre {
					white-space: pre-wrap;       /* css-3 */
					white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
					white-space: -pre-wrap;      /* Opera 4-6 */
					white-space: -o-pre-wrap;    /* Opera 7 */
					word-wrap: break-word;       /* Internet Explorer 5.5+ */
					background: rgba(0,0,0,.05);
				    font-size: 16px;
				    padding: 20px;
				    color: rgba(0,0,0,.84);
				    line-height: 1.4;
				}
				.content p {
					font-size: 18px;
					font-family: 'Vollkorn', serif;
				    word-spacing: 2px;
				}
				.tab_list ul {
					margin: 0px;
					padding: 0px;
				}
				.tab_list ul li{
				    list-style-type: none;
				    display: inline-block;
				    background-color: rgba(0,0,0,.05);
				    color: #000000ad;
				    font-size: 15px;
				    -webkit-border-radius: inherit;
				    border-radius: 3px;
				    border: 1px solid #f0f0f0;
				    padding: 5px 10px;
				    margin: 10px 10px 10px 0px;
				}
			`}</style>
		</div>
		</Layout>
)

Post.getInitialProps = async function(context) {
	const {slug} = context.query
	const color_array = ['#663399','#674172','#4183D7','#013243','#3A539B']
	const single = await posts.getEntries({content_type: '2PqfXUJwE8qSYKuM0U6w8M','fields.slug': slug})
	console.log(single.items[0])
	return {single_post: single.items[0]}
}

export default Post