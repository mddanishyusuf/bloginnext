import Layout from '../components/Layout'
import PostCard from '../components/PostCard'
import fetch from 'isomorphic-unfetch'
import * as contentful from 'contentful'

const posts = contentful.createClient({
  space: '5yxy7n108xih',
  accessToken: 'db255a839faee204bbbd5ff96e5c8f7c784d12b3d025cb264af5148057a8812e'
})

const Index = (props) => (
	<Layout title="IAMNEWBIE - A Talk About Open Source Projects, Tools &amp; APIs">
		<div className="main">
			{props.posts.map((post, index) => (
				<PostCard data={post} key={post.sys.id}/>
			))}
		</div>
		<div className="pagination">
			<ul>
				<li>1</li>
				<li>2</li>
				<li>3</li>
			</ul>
		</div>
	    <style jsx>{`
	      h1, a {
	        font-family: "Arial";
	      }
	      *, *:before, *:after {
	      	box-sizing: border-box;
	      }
	      body {
	      	padding:0px;
	      	margin:0px;
	      }
	      ul {
	        padding: 0;
	      }

	      li {
	        list-style: none;
	        margin: 5px 0;
	      }

	      a {
	        text-decoration: none;
	        color: blue;
	      }

	      a:hover {
	        opacity: 0.6;
	      }
	      .main {
	      	width:70%;
	      	margin:auto;
	      	margin-top:40px;
	      }
	      .pagination {
	      	text-align: center;
	      }
	      .pagination ul {
	      	margin: 0px;
	      	padding: 0px;
	      }
	      .pagination ul li {
	      	list-style-type: none;
	      	display: inline-block;
      	    background-color: #eee;
		    margin: 4px;
		    padding: 3px 11px;
		    cursor: pointer;
		    border-radius: 50%;
	      }
	    `}</style>
	</Layout>
)

Index.getInitialProps = async function() {
	const res = await posts.getEntries({content_type: '2PqfXUJwE8qSYKuM0U6w8M'})
	return {
		posts:res.items
	}
}

export default Index