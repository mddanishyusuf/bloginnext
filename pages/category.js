import Layout from '../components/Layout'
import PostCard from '../components/PostCard'
import posts from '../services/contentconf'
import Link from 'next/link'

const Category = (props) => (
		<Layout title={props.title}>
		<div className="category_div">
		<div className="breadcrumb"><Link href="/">Home</Link> / {props.category_name}</div>
			{props.posts.map((posts, index) => (
				<PostCard data={posts} key={index}/>
			))}
		</div>
		<style jsx global>{`
			.category_div {
				width: 70%;
				margin:0px auto;
			}
			.breadcrumb {
				padding: 20px;
			}
		`}</style>
	</Layout>
)

Category.getInitialProps = async function(context) {
	const cid = context.query.cid
	const title = context.query.title
	const data = await posts.getEntries({content_type: '2PqfXUJwE8qSYKuM0U6w8M','fields.categories.sys.id[in]':cid})
	const capitalize_title = title.charAt(0).toUpperCase() + title.slice(1) + ' Resources | IAMNEWBIE.COM';
	return {
		posts:data.items,
		title:capitalize_title,
		category_name: title
	}
}

export default Category