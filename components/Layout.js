import Header from './Header'
import Head from 'next/head'

const Layout = (props) => (
	<div>	
	    <Head>
		<title>{ props.title }</title>
		<meta charSet="utf-8"/>
		<meta name="author" content="Mohd Danish Yusuf"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"/>
		<link href="https://fonts.googleapis.com/css?family=Vollkorn" rel="stylesheet"/>
		<style>{`
		body {
			margin: 0;
			font-family: 'Vollkorn', serif;
		}
		`}</style>
	    </Head>
		<Header/>
		{props.children}
	</div>
)

export default Layout