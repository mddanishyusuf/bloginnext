import Link from 'next/link'

const PostLink = (props) => (
		<Link as={`/post/${props.data.fields.slug}`} href={`/post?slug=${props.data.fields.slug}`}>
		<a>
			<div className="post_card">
				<div className="post_card_padding">
					<div className="post_card_inner">
						<div className="post_card_content">
							<div className="post_featured_media" style={{'backgroundImage': `url(${props.data.fields.image[0].fields.file.url})`,'height':'200px','backgroundSize': 'cover','backgroundPosition':'center'}}></div>
							<div className="post_title">{props.data.fields.productName}</div>
						</div>
						<div className="post_link">Read More -></div>
					</div>
				</div>
			<style jsx global>{`
				.post_card {
					width: 33%;
					display:inline-block;
				}
				.post_card_padding {
					padding:10px;
					cursor:pointer;					
				}
				.post_card_inner {
				    border-radius: 2px;
				    box-shadow: 0 2px 4px rgba(0,0,0,0.15);
				}
				.post_title {
				    font-size: 17px;
				    margin-bottom: 5px;
				    margin-top: 0;
				    padding: 15px 20px 0;
				    height: 40px;
				}
				.post_link {
					color:#3266D5;
					font-size:14px;
					font-weight:500;
					padding: 20px;
				}
			`}
			</style>
			</div>
			</a>
		</Link>
)

export default PostLink