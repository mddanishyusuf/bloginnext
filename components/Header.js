import Link from 'next/link'

const Header = () => (
	<div className="navbar">
		<Link href="/"><a>
		<div className="logo_div">
			<div className="logo_title">I<span className="logo_am">AM</span>NEWBIE</div>
			<small className="logo_sub">A Talk About Open Source Projects, Tools & APIs</small>
		</div></a>
		</Link>
		<div className="search_box"></div>
		<div className="nav_menu">
			<Link as="/category/react/16nvnLLBJIWEsggamo6se4" href="/category?title=react&cid=16nvnLLBJIWEsggamo6se4"><a>React</a></Link>
			<Link as="/category/angular/1mCzlnUpLyOMmy2c0KSO04" href="/category?title=angular&cid=1mCzlnUpLyOMmy2c0KSO04"><a>Angular</a></Link>
			<Link as="/category/tools/5bU0nqoRS0eIWOQ0ygSqGW" href="/category?title=tools&cid=5bU0nqoRS0eIWOQ0ygSqGW"><a>Tools</a></Link>
			<Link href="/about"><a>About</a></Link>
			<Link href="/contact"><a>Contact</a></Link>
		</div>
		<style jsx>{`
			.logo_div {
				display:inline-block;
				vertical-align:middle;
				padding-right:100px;
				cursor:pointer;
			}
			.navbar {
				margin: 0 auto;
			    padding: 14px 50px;
			    background-color: #F6F6F6;
			    box-shadow: 0 2px 5px 0 rgba(0,0,0,0.2);
			}
			.search_box{
				display:inline-block;
				width:600px;
				vertical-align:middle;				
			}
			.logo_title {
				font-size: 25px;
				color:#393737;
			}
			.logo_sub{				
			    color: #747474;
			}
			.logo_am {
				background-color:#5570ff;
				color:#fff;
				padding:0px 10px
			}
			.nav_menu {				
				display:inline-block;
				vertical-align:middle
			}
			.navbar a{
			    color: #747474;
			    display: inline-block;
			    font-weight: 400;
			    list-style: none;
			    margin: 0 16px;
			    vertical-align: baseline;
			    text-decoration: none;
			}
		`}			
		</style>
	</div>
)

export default Header